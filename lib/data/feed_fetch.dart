class FeedFetch {
  // subscribing last todo added in database
  // gives only the "last added" element in the database

  // Starts a subscription to the last todo in the database
  // Whenever data is received, it looks at the todos and compare previous id with current id via subscription,
  // if new id is greater then previous id, it will increase the count of notification.
  static String fetchNewNotification = """subscription fetchNewNotification {
  todos(where: {is_public: {_eq: true}}, limit: 1, order_by: {created_at: desc}) {
    id
  }
}
""";

  // subscription above can be treated as an event notifcation
  // saying "something has been added" and fetch all todos from the database
  // latest with respect to your todo present in the local cache

  static String addPublicTodo = """mutation (\$title: String!){
    insert_todos (
      objects: [{
        title: \$title,
        is_public: true
      }]
    ){
      returning {
        id
      }
    }
  }""";

  static String loadMoreTodos = """ query loadMoreTodos (\$oldestTodoId: Int!) {
       todos (where: { is_public: { _eq: true}, id: {_lt: \$oldestTodoId}}, limit: 7, order_by: {
             created_at: desc }) {
         id
         title
         created_at
         user {
           name
         }
       }
     }""";

  static String newTodos = """query newTodos (\$latestVisibleId: Int!) {
      todos(where: { is_public: { _eq: true}, id: {_gt: \$latestVisibleId}}, order_by: { created_at:
      desc }) {
        id
        title
        created_at
        user {
          name
        }
      }
    }""";
}
