import 'package:flutter/cupertino.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class Config {
  static String _token;

  // Setup a GraphQL client to use the endpoint
  static final HttpLink httpLink =
      HttpLink(uri: 'https://hasura.io/learn/graphql');

  static final AuthLink authLink = AuthLink(getToken: () => _token);

  static final WebSocketLink webSocketLink = WebSocketLink(
    url: 'wss://hasura.io/learn/graphql',
    config: SocketClientConfig(
      autoReconnect: true,
      inactivityTimeout: Duration(seconds: 30),
      initPayload: {
        'headers': {'Authorization': _token},
      },
    ),
  );

  static final Link link = authLink.concat(httpLink).concat(webSocketLink);

  // configuring client using
  static ValueNotifier<GraphQLClient> initializeClient(String token) {
    _token = token;
    ValueNotifier<GraphQLClient> client = ValueNotifier(GraphQLClient(
        cache: OptimisticCache(dataIdFromObject: typenameDataIdFromObject),
        link: link));
    return client;
  }
}
